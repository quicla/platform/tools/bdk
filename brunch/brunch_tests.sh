#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# This script is a simple harness for executing shell commands from
# the tests/ subdir.
#
# See tests/README for more details.

GetPath() {
  local _path=$(readlink -f $1)
  local _brunch_dir=$(dirname $_path)
  echo ${_brunch_dir}
}

brunch_root=$(GetPath $0)

cd ${brunch_root}
pass=0
fail=0
tests=`ls tests/*.sh 2>/dev/null`
rm -f test.1.log test.2.log
for t in $tests; do
  echo -n "Running '${t}' . . . "
  echo "===== $t =====" >> test.1.log
  echo "===== $t =====" >> test.2.log
  if bash $t >> test.1.log 2>> test.2.log; then
    echo ok
    pass=$((pass+1))
  else
    echo fail
    fail=$((fail+1))
  fi
done

total=$((pass+fail))

echo "$pass/$total tests passed."
echo "$fail/$total tests failed."
exit $fail
