#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

stub_git
stub_tar
stub_curl

# Set up isolation for testing.
export HOME=${PWD} # bsps live in "~/.brillo/.BSPs".
mkdir bdk

common_args=("-m" "${DATA_DIR}/test_manifest.json" "-b" "bdk")

echo "Testing bsp status"

# A trivial bsp is "Installed".
${BRUNCH} bsp status void_board "${common_args[@]}" | grep "Void Board - Installed"

# Start uninstalled.
${BRUNCH} bsp status test_board "${common_args[@]}" | grep "Test Board - Not Installed"
# Install.
${BRUNCH} bsp install test_board --accept_licenses "${common_args[@]}"
${BRUNCH} bsp status test_board "${common_args[@]}" | grep "Test Board - Installed"
# Something unexpected in tree.
rm bdk/bsp/tar1
touch bdk/bsp/tar1
${BRUNCH} bsp status test_board "${common_args[@]}" | grep "Test Board - Unrecognized"
# Use "wrong version".
mkdir wrong_bsp
rm bdk/bsp/tar2
ln -s ${PWD}/wrong_bsp bdk/bsp/tar2
${BRUNCH} bsp status test_board "${common_args[@]}" | grep "Test Board - Incorrect Version"
# Screw up tree.
rm bdk/bsp/git/git_subpackage
touch target
ln -s target bdk/bsp/git/git_subpackage
rm target
${BRUNCH} bsp status test_board "${common_args[@]}" | grep "Test Board - Invalid"
