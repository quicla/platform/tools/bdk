#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

stub_git
stub_tar
# We don't want curl to happen.
stub_dead_curl

# Set up isolation for testing.
export HOME=${PWD} # bsps live in "~/.brillo/.BSPs".
mkdir bdk

echo "Testing brunch bsp install"
# Note: download, update, and install should all be aliases for the same thing.
# This tests installing from an already present tarball, bsp_download tests download
# starting from nothing in the bdk tree, bsp_update.sh tests updating a messed up bdk tree.

${BRUNCH} bsp install test_board -a -m "${DATA_DIR}/test_manifest.json"\
 -e "${DATA_DIR}/package_tree/tarball.tar.gz" -b bdk > output.txt

# All checks are obviously dependent upon how test_board is defined in test_manifest.

# Licenses should have been read.
grep "git subpackage license" output.txt
grep "tar license 1" output.txt
grep "tar license 2" output.txt

# Success message expected.
grep "Successfully installed all packages for Test Board." output.txt
# Make sure links were created.
grep "git subpackage license" bdk/bsp/git/git_subpackage/git_license.txt
grep "tar license 1" bdk/bsp/tar1/tar_license1.txt
grep "tar license 2" bdk/bsp/tar2/deeper/tar_license2.txt
