#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for classes in device.py"""

import unittest

from test import stubs
import package_stub
import device
import package
import status
import subpackage

class DeviceTest(unittest.TestCase):
  def setUp(self):
    self.base_device = {
      'device_name' : 'Test Device 1',
      'packages' : {
        'package_1' : {
          'subpackage_1' : 'path/to/link'
        }
      }
    }
    self.package1 = package_stub.StubPackage('package_1', subpackages = {
      'subpackage_1' : status.INSTALLED,
      'subpackage_2' : status.INSTALLED
    })
    self.package2 = package_stub.StubPackage('package_2', subpackages = {
      'subpackage_2' : status.INSTALLED
    })
    self.packages = { 'package_1' : self.package1, 'package_2' : self.package2 }

    self.stub_os = stubs.StubFactory.new(stubs.StubOs)
    self.stub_open = stubs.StubFactory.new(stubs.StubOpen)
    self.stub_open.os = self.stub_os
    self.stub_hashlib = stubs.StubFactory.new(stubs.StubHashlib)
    self.stub_stdout = stubs.StubStdout()

    package.os = self.stub_os
    package.open = self.stub_open.open
    package.hashlib = self.stub_hashlib
    package.sys.stdout = self.stub_stdout

    self.dev = device.Device('device', { self.package1 : { 'subpackage_1' : 'path1',
                                                           'subpackage_2' : 'path2' },
                                         self.package2 : { 'subpackage_2' : 'path3' } })

  def test_from_dict(self):
    dev = device.Device.FromDict(self.base_device, self.packages)
    self.assertIsInstance(dev, device.Device)
    self.assertTrue(self.package1 in dev._package_map)
    self.assertEqual(dev._package_map[self.package1]['subpackage_1'], 'path/to/link')

  def test_from_dict_bad_package(self):
    incomplete_device = self.base_device
    incomplete_device['packages']['nonexistent'] = {}
    self.assertRaisesRegexp(ValueError, 'Package.*nonexistent.*does not exist',
                            device.Device.FromDict, incomplete_device, self.packages)

  def test_from_dict_bad_subpackage(self):
    incomplete_device = self.base_device
    incomplete_device['packages']['package_1']['nonexistent'] = {}
    self.assertRaisesRegexp(ValueError, 'Sub.*nonexistent.*does not exist',
                            device.Device.FromDict, incomplete_device, self.packages)

  def test_status_installed(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INSTALLED
    self.package2.subpackages['subpackage_2'] = status.INSTALLED
    self.assertEqual(self.dev.Status()[0], status.INSTALLED)

  def test_status_unrecognized(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INSTALLED
    self.package2.subpackages['subpackage_2'] = status.UNRECOGNIZED
    self.assertEqual(self.dev.Status()[0], status.UNRECOGNIZED)

  def test_status_incorrect_version(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INCORRECT_VERSION
    self.package2.subpackages['subpackage_2'] = status.UNRECOGNIZED
    # Also test that this beats out unrecognized.
    self.assertEqual(self.dev.Status()[0], status.INCORRECT_VERSION)

  def test_status_not_installed(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.NOT_INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INCORRECT_VERSION
    self.package2.subpackages['subpackage_2'] = status.UNRECOGNIZED
    # Also test that this beats out incorrect version and unrecognized.
    self.assertEqual(self.dev.Status()[0], status.NOT_INSTALLED)

  def test_status_invalid(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.NOT_INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INCORRECT_VERSION
    self.package2.subpackages['subpackage_2'] = status.INVALID
    # Also test that this beats out incorrect version and not installed
    # (and transitively unrecognized).
    self.assertEqual(self.dev.Status()[0], status.INVALID)

  def test_cleanup(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.NOT_INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INCORRECT_VERSION
    self.package2.subpackages['subpackage_2'] = status.INVALID
    self.package2.subpackages['subpackage_3'] = status.INSTALLED
    self.package2.subpackages['subpackage_4'] = status.UNRECOGNIZED
    self.package1.should_remove = ['subpackage_1', 'subpackage_2']
    self.package2.should_remove = ['subpackage_2']
    self.dev.Cleanup()
    # Should clean up invalid and incorrect_version, but not installed or unrecognized.
    self.assertEqual(self.package1.SubPackageStatus('subpackage_1', '')[0], status.NOT_INSTALLED)
    self.assertEqual(self.package1.SubPackageStatus('subpackage_2', '')[0], status.NOT_INSTALLED)
    self.assertEqual(self.package2.SubPackageStatus('subpackage_2', '')[0], status.NOT_INSTALLED)
    self.assertEqual(self.package2.SubPackageStatus('subpackage_3', '')[0], status.INSTALLED)
    self.assertEqual(self.package2.SubPackageStatus('subpackage_4', '')[0], status.UNRECOGNIZED)

  def test_unrecognized_paths(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.NOT_INSTALLED
    self.package1.subpackages['subpackage_2'] = status.UNRECOGNIZED
    self.package2.subpackages['subpackage_2'] = status.UNRECOGNIZED
    result = self.dev.UnrecognizedPaths()
    self.assertIn('path2', result)
    self.assertIn('path3', result)
    self.assertNotIn('path1', result)

  def test_match_tarball(self):
    matching = package.TarPackage('match', None, '<correct_version>', None)
    non_matching_tar = package.GitPackage('non_match', None, '<wrong_version>', None)
    # While unlikely git and tar hashes would match, it is in theory possible.
    # Luckily the branch:hash formulation for git versions should prevent this issue.
    non_matching_git = package.TarPackage('git', None, '<branch>:<correct_version>', None)
    self.stub_open.files = { 'file1' : stubs.StubFile('file1') }
    self.stub_os.path.should_exist = ['file1']
    self.stub_hashlib.should_return = '<correct_version>'
    dev = device.Device('name', {non_matching_tar:{}, matching:{}, non_matching_git:{}})
    self.assertEqual(dev.MatchTarball('file1'), 'match')

  def test_non_matching_tarball(self):
    non_matching_tar = package.TarPackage('non_match', None, '<wrong_version>', None)
    # While unlikely git and tar hashes would match, it is in theory possible.
    # Luckily the branch:hash formulation for git versions should prevent this issue.
    non_matching_git = package.GitPackage('git', None, '<branch>:<correct_version>', None)
    self.stub_open.files = { 'file1' : stubs.StubFile('file1') }
    self.stub_os.path.should_exist = ['file1']
    self.stub_hashlib.should_return = '<correct_version>'
    dev = device.Device('name', {non_matching_tar:{}, non_matching_git:{}})
    self.assertEqual(dev.MatchTarball('file1'), None)

  def test_install(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.NOT_INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INSTALLED
    self.package2.subpackages['subpackage_2'] = status.INSTALLED
    self.package1.should_download = True
    self.package2.should_download = False # The only subpackage is already installed.
    self.package1.should_link['subpackage_1'] = status.INSTALLED
    self.assertTrue(self.dev.Install())

  def test_void_install(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INSTALLED
    self.package2.subpackages['subpackage_2'] = status.INSTALLED
    self.package1.should_download = False
    self.package2.should_download = False
    self.assertTrue(self.dev.Install())

  def test_install_unrecognized(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.NOT_INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INSTALLED
    self.package2.subpackages['subpackage_2'] = status.UNRECOGNIZED
    self.package1.should_download = True
    self.package2.should_download = False # Still shouldn't download over unrecognized.
    self.package1.should_link['subpackage_1'] = status.INSTALLED
    self.assertTrue(self.dev.Install())

  def test_install_failed_download(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.NOT_INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INSTALLED
    self.package2.subpackages['subpackage_2'] = status.INSTALLED
    self.package1.should_download = False
    self.package2.should_download = False
    self.package1.should_link['subpackage_1'] = status.INSTALLED
    self.assertFalse(self.dev.Install())

  def test_install_failed_link(self):
    self.package1.subpackages = {}
    self.package2.subpackages = {}
    self.package1.subpackages['subpackage_1'] = status.NOT_INSTALLED
    self.package1.subpackages['subpackage_2'] = status.INSTALLED
    self.package2.subpackages['subpackage_2'] = status.INSTALLED
    self.package1.should_download = True
    self.package2.should_download = False
    self.package1.should_link['subpackage_1'] = status.INVALID
    self.assertFalse(self.dev.Install())

  def test_refresh(self):
    self.package1.should_refresh = True
    self.package2.should_refresh = True
    self.assertTrue(self.dev.Refresh())

  def test_failed_refresh(self):
    self.package1.should_refresh = True
    self.package2.should_refresh = False
    self.assertFalse(self.dev.Refresh())
