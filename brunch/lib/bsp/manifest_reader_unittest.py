#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for the bsp manifest reader in manifest_reader.py"""

import json
import unittest

from test import stubs
import manifest
import manifest_reader

class ManifestReaderTest(unittest.TestCase):
  def setUp(self):
    self.stub_os = stubs.StubFactory.new(stubs.StubOs)
    self.stub_open = stubs.StubFactory.new(stubs.StubOpen)
    self.stub_open.os = self.stub_os

    self.base_manifest = {
      'packages' : {
        'package_1' : {
          'package_type' : 'git',
          'remote' : 'remote',
          'version' : 'version',
          'subpackages' : {
            'subpackage_1' : {
              'subdir' : '.',
              'licenses' : ['license1', 'license2']
            }
          }
        }
      },
      'devices' : {
        'device_1' : {
          'device_name' : 'Test Device 1',
          'packages' : {
            'package_1' : {
              'subpackage_1' : 'path/to/extract'
            }
          }
        }
      }
    }

    manifest_reader.os = self.stub_os
    manifest_reader.open = self.stub_open.open

    self.manifest_path = 'manifest'
    self.stub_os.path.should_exist.append(self.manifest_path)

  def test_basic_manifest(self):
    self.stub_open.files[self.manifest_path] = stubs.StubFile(json.dumps(self.base_manifest))
    man = manifest_reader.Read(self.manifest_path)
    self.assertIsInstance(man, manifest.Manifest)

  def test_non_json_manifest(self):
    self.stub_open.files[self.manifest_path] = stubs.StubFile('{ unmatched bracket is invalid json')
    self.assertRaisesRegexp(ValueError, 'json', manifest_reader.Read, self.manifest_path)

  def test_incomplete_manifest(self):
    incomplete_manifest = self.base_manifest
    incomplete_manifest['devices']['device_2'] = {}
    self.stub_open.files[self.manifest_path] = stubs.StubFile(json.dumps(incomplete_manifest))
    # Hopefully the error indicates that the issue is somewhere in device_2
    self.assertRaisesRegexp(KeyError, 'device_2', manifest_reader.Read, self.manifest_path)

  def test_invalid_manifest(self):
    invalid_manifest = self.base_manifest
    invalid_manifest['devices']['device_1']['packages']['package_1']['subpackage_2'] = 'path'
    self.stub_open.files[self.manifest_path] = stubs.StubFile(json.dumps(invalid_manifest))
    # Error should indicate that subpackage_2 is not a defined subpackage of package_1.
    self.assertRaisesRegexp(ValueError, 'subpackage_2', manifest_reader.Read, self.manifest_path)

  def test_manifest_not_found(self):
    self.stub_os.path.should_exist = []
    # Some sort of error about the file.
    self.assertRaisesRegexp(IOError, self.manifest_path, manifest_reader.Read, self.manifest_path)
