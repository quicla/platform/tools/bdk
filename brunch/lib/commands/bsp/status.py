#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Get details for a specific BSP."""

from bsp import manifest_reader
from bsp import status
from cli import clicommand

class Status(clicommand.Command):
  """Get detailed information on the current status of a device BSP."""

  @staticmethod
  def Args(parser):
    parser.add_argument('device', help='Device to check the BSP status of.')

  def Run(self, args):
    manifest = manifest_reader.Read(args.manifest, args.bdk)

    device = manifest.devices.get(args.device)
    if not device:
      print 'Unrecognized device name:', args.device
      return 1

    print device.Status(verbose=True)[1]
    return 0
