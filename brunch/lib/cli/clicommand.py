#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Brunch CLI command classes"""

class Command(object):
  @staticmethod
  def Args(parser):
    pass

  @classmethod
  def set_group(cls, grp):
    cls._group = grp

  @classmethod
  def group(cls):
    try:
      return cls._group
    except AttributeError, e:
      return None

  def Run(self, args):
    """Called when the command is used. Must return an 'int'."""
    raise NotImplementedError, 'Run() must be overridden'

class Group(object):
  @staticmethod
  def GroupArgs(parser):
    """Set args that are available for all commands in a group."""
    pass
