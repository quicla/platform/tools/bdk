#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""A module to send hits, saving them if they fail to send.

This module defines three methods, SendHit, SendRetries and SendHitAndRetries.
The first attempts to send a hit, and saves it if it fails. The second
retries sending all previously failed hits (saving them again if they continue
to fail, otherwise removing their saved data). The third combines the two,
sending a hit, and, if it succeeds, trying to send the retries as well.

Example use:

  success = SendHit(hit)

  if not success:
    SendRetries()
"""

import hit_store
from data_types.generalized_ga import hit

def SendHit(hit_obj):
  """Sends a hit, saving if it fails.

  Args:
    hit_obj: a generalized_ga.Hit object

  Returns:
    True if the Send succeeds, False otherwise.
  """
  return SendHitFields(hit_obj.GetFields())


def SendHitFields(hit_fields):
  """Sends a hit, saving if it fails.

  Args:
    hit_fields: A dictionary of { key : val } describing the hit.
  """
  result = False
  try:
    result = hit.Hit.SendFields(hit_fields)
  finally:
    if not result:
      hit_store.Backup().Save(hit_fields)
  return result


def SendHitAndRetries(hit_obj):
  """Sends a hit, saving if it fails, retrying all others if it succeeds.

  The idea is sending probably fails due to a poor network connection. If this
  send succeeded, then others might too.

  Args:
    hit: a generalized_ga.Hit object

  Returns:
    True if the Send succeeds, False otherwise.
  """
  result = SendHit(hit_obj)
  if result:
    SendRetries()
  return result


def SendRetries():
  """Retries all previously failed sends.

  If they fail again, they will be re-saved for the *next* time SendRetries
  is called.
  """
  for hit_fields in hit_store.Backup().RetrieveAll():
    SendHitFields(hit_fields)
